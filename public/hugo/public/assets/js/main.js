jQuery(document).ready(function($) {
    var timelineBlocks = $('.cd-timeline-block'),
        offset = 0.8;

    //hide timeline blocks which are outside the viewport
    hideBlocks(timelineBlocks, offset);

    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function() {
        (!window.requestAnimationFrame) ?
        setTimeout(function() {
            showBlocks(timelineBlocks, offset);
        }, 100): window.requestAnimationFrame(function() {
            showBlocks(timelineBlocks, offset);
        });
    });

    function hideBlocks(blocks, offset) {
        blocks.each(function() {
            ($(this).offset().top > $(window).scrollTop() + $(window).height() * offset) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
        });
    }

    function showBlocks(blocks, offset) {
        blocks.each(function() {
            ($(this).offset().top <= $(window).scrollTop() + $(window).height() * offset && $(this).find('.cd-timeline-img').hasClass('is-hidden')) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
        });
    }


    $("#content").click(function(e) {
        balls.push(new Ball(e.pageX - e.target.offsetLeft, e.pageY - e.target.offsetLeft));
    });

});



var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth * 0.98;
canvas.height = window.innerHeight * 0.9;
var ctx = canvas.getContext("2d");

var TAU = 2 * Math.PI;

times = [];

function loop() {
    var startTime = performance.now();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    update();
    draw();
    times.push(performance.now() - startTime);
    if (times.length > 500) {
        times.shift()
    }

    requestAnimationFrame(loop);
}

function Ball(startX, startY, startVelX, startVelY) {
    this.x = startX || Math.random() * canvas.width;
    this.y = startY || Math.random() * canvas.height;
    this.vel = {
        x: startVelX || Math.random() * 2 - 1,
        y: startVelY || Math.random() * 2 - 1
    };
    this.update = function(canvas) {
        if (this.x > canvas.width + 50 || this.x < -50) {
            this.vel.x = -this.vel.x;
        }
        if (this.y > canvas.height + 50 || this.y < -50) {
            this.vel.y = -this.vel.y;
        }
        this.x += this.vel.x / 6;
        this.y += this.vel.y / 6;
    };
    this.draw = function(ctx, can) {
        ctx.beginPath();
        if (distMouse(this) > 100) {
            ctx.fillStyle = "#8f9aa3";
            ctx.globalAlpha = .2;
        } else {
            ctx.fillStyle = '#448fda';
            ctx.globalAlpha = .6;
        }
        ctx.arc((0.5 + this.x) | 0, (0.5 + this.y) | 0, 3, 0, TAU, false);
        ctx.fill();
    }
}

var balls = [];
for (var i = 0; i < canvas.width * canvas.height / (85 * 85); i++) {
    balls.push(new Ball(Math.random() * canvas.width, Math.random() * canvas.height));
}

var lastTime = Date.now();

function update() {
    var diff = Date.now() - lastTime;
    for (var frame = 0; frame * 16.6667 < diff; frame++) {
        for (var index = 0; index < balls.length; index++) {
            balls[index].update(canvas);
        }
    }
    lastTime = Date.now();
}

var mouseX = -1e9,
    mouseY = -1e9;
document.addEventListener('mousemove', function(event) {
    mouseX = event.clientX;
    mouseY = event.clientY;
});

function distMouse(ball) {
    return Math.hypot(ball.x - mouseX, ball.y - mouseY);
}

function draw() {
    for (var index = 0; index < balls.length; index++) {
        var ball = balls[index];
        ball.draw(ctx, canvas);
        ctx.beginPath();
        for (var index2 = balls.length - 1; index2 > index; index2 += -1) {
            var ball2 = balls[index2];
            var dist = Math.hypot(ball.x - ball2.x, ball.y - ball2.y);
            if (dist < 100) {
                if (distMouse(ball) > 100) {
                    ctx.strokeStyle = "#8f9aa3";
                    ctx.globalAlpha = .2;
                } else {
                    ctx.strokeStyle = '#448fda';
                    ctx.globalAlpha = .6;
                }
                ctx.lineWidth = "2px";
                ctx.moveTo((0.5 + ball.x) | 0, (0.5 + ball.y) | 0);
                ctx.lineTo((0.5 + ball2.x) | 0, (0.5 + ball2.y) | 0);
            }
        }
        ctx.stroke();
    }
}

// Start
loop();

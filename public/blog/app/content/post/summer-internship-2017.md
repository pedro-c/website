---
title: Summer Internship at Bitmaker Software 2017
author: "Pedro Costa"
tags: ["Bitmaker", "Summer Internship", "Internship"]
date: 2018-01-21
---

### Why make a summer internship at all?
There were three reasons that lead me to search for a summer internship:

- First of all, I wanted to put in practice what I've learned at the university so far, put my skills to the test.
- Secondly, learn new things, new technologies, find out what is actually used in the market to foster the real services that we use every day.
- At last, acquire professional experience, working in a professional environment using development methodologies such as Agile.

<!--more-->

#### So what to do? 
Quite easy actually, I just sent e-mails to a few companies, in Porto, asking if they were interested in me working for a couple of months and attached my CV.

Et voilá, Bitmaker presented itself. On the first interview, at the Founders Founders Caffée, they asked for my interests, background in programming and we discussed a few of their projects. I end up choosing to integrate the team that was developing a web platform for benefits management. It was the first time I heard of using a functional programming language in web development for a real use case scenario and that caught my attention.

#### The first two days
On the first two days, I was presented to the team and given access to their code repository, company e-mail, slack, etc. But most importantly they gave me two books, [Programming Phoenix](https://pragprog.com/book/phoenix/programming-phoenix) and [Programming Elixir](https://pragprog.com/book/elixir/programming-elixir), at first I was a bit lost and went just through the books trying to learn as fast as I could about the framework and the functional language elixir. The more I read the more interesting Phoenix and Elixir seemed to be, Phoenix is amazing with a lot of features and simple routing system powered by plugs (aka middlewares).


#### The fun part
On the third day, I told Tiago Fernandes (CTO of Bitmaker) that I was confident enough to get my hands on the project and start contributing to it. And this is where things became interesting. Bitmaker takes Agile quite serious, Tiago had all the user stories on Jira, all in the correct format that we are told to use at university but never thought that companies actually took it that seriously. 

Almost every day, the whole team had a daily scrum session at 11 am, one by one we told what we had done and what we were going to do, and this was critical to keep everyone motivated, there was a great environment where if one was struggling with some feature others would share their thoughts and offer help after the session.

The agile method worked so well that every day when I came to the office I didn't even have to ask what I had to do, just opened Jira and started working.
Special thanks to Paulo and Daniel who were always keen on helping no matter how many times a day I asked for and there was always time for breaks at the ping pong table and coffee whenever work required.

Every month we went out to have "Francesinha" for a team lunch so there was really a pleasant and welcoming environment.

The thing that I actually enjoyed the most was the meetings at the whiteboard. Whenever the team had to make a decision about the project, Tiago always invited everyone (even the intern) to join him around the whiteboard and think how to solve the issue, it was really a collaborative environment. My opinion was always heard and taken into account as well as everyone else. Tiago always had in mind problems such as scalability and performance and he was always open to discuss any development on the project with me.


#### Was it worth the time?
With hundred percent certainty, I can say it was. I learned a lot about web development, how to properly work with a REST API backend and the Phoenix Framework powered by the Elixir language as well as how the agile methods work and how to distribute tasks across a team and efficiently communicate and support each other. Both skills were very important later in a university project where we used Phoenix and Elixir for developing a web platform and where we were evaluated by the proper use of the Agile methods.

### Bonus
Bitmaker keeps a nice shelf with excellent books such as "Computer Networks" and "Distributed Systems: Principles and Paradigms" both by Andrew Tanenbaum and "Building Microservices" by Sam Newman. The later I took it home for the duration of the internship and I definitely advise everyone to read! As well as the previous two which I had the opportunity to study at university.

### Summing it up
It was a rewarding experience, surrounded by a great CTO and team, with exceptional books, free coffee, ping pong partners for the breaks, excellent whiteboard discussions and an outstanding choice of framework and language. Not to forget, the design team and CEO which were both friendly and always keen to help!
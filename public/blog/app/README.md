# blog


## Run

In order to see your site in action, run Hugo's built-in local server.

```bash
$ hugo server -w
```

Now enter [localhost:1313](http://localhost:1313) in the address bar of your browser.


## Build

```bash
# Install dependences
$ cd themes/hugo-nuo
$ npm install

# Scripts dev
$ npm run dev
$ npm run build

# Styles dev
$ npm run sass

# Copy fonts to static
$ npm run fonts

# Copy images to static
$ npm run images

# Scripts lint
$ npm run eslint

# Styles lint
$ npm run stylelint

# Minify images
$ npm run imagemin
```
